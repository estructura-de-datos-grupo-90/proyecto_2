#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50

void abrirURL(char *URL){
  // Abre en una pestaña del navegador predeterminado del usuario el URL ingresado.
  char llamada[48] = "xdg-open http://";
  strcat(llamada, URL);
  printf("%s",llamada);
  system(llamada);
}

char *eliminarSobrante(char *cadena){
  //Elimina caracteres los caracteres " , de la cadena ingresada. Retorna la nueva cadena sin estos caracteres 
  char *cadena_temporal = malloc(strlen(cadena)+1);
  int j = 0;
  for (int i=0; i<strlen(cadena); i++){
    switch (cadena[i]){
      case ',': break;
      case '\"': break;
      default:
      cadena_temporal[j] = cadena[i];
      j++;
    }
  }
  cadena_temporal[j] = '\0';
  return cadena_temporal;
}


char* get_user_input(size_t max_size){
  //Lee el string que el usuario ingresa en la terminal y lo retorna.
  char *buffer;
  size_t characters;
  buffer = (char *)malloc(max_size * sizeof(char));
  if(buffer == NULL){
    perror("ERROR No fue posible reservar memoria para el buffer");
    exit(1);
  }
  characters = getline(&buffer,&max_size,stdin);
  buffer[strlen(buffer)-1]= 0;
  return buffer;
}

int get_user_numerical_input(size_t max_size){
  //Retorna como int lo que el usuario ingresa en la terminal.
	int numerical_input = atoi(get_user_input(max_size));
	return numerical_input;
}

//Declaración de nodo
typedef struct nodo {
  char* url;
  char* alias;
  struct nodo *padre;
  struct nodo *izq;
  struct nodo *der;
}nodo;

nodo *raiz=NULL;

void insertar(char* nuevo_alias, char* nuevo_url){
  //Inserta un nodo en el arbol con los datos de entrada.
  nuevo_url = eliminarSobrante(nuevo_url); //Elimina caracteres " , de la hilera.
  int antes_siguente;
  int antes_siguente1;
  struct nodo *nuevo; //Nodo a insertar
  nuevo = malloc(sizeof(struct nodo));
  nuevo->alias = nuevo_alias; //Asignación del alias de entrada al nodo a insertar
  nuevo->url = nuevo_url; //Asignación del url de entrada al nodo a insertar
  nuevo->padre = NULL;
  nuevo->izq = NULL;
  nuevo->der = NULL;
  if (raiz == NULL){
    raiz = nuevo;
    raiz->padre = NULL;
  }else{
    struct nodo *anterior, *reco;
    anterior = NULL;
    reco = raiz;
    while (reco != NULL){
      anterior = reco;
      antes_siguente = strcmp(nuevo->url ,reco->url );
      if (0 >= antes_siguente){
        reco = reco->izq;
      }else{
        reco = reco->der;
      }
    }
    antes_siguente = strcmp(nuevo->url, anterior->url);
    if (0 >= antes_siguente){
      anterior->izq = nuevo;
      nuevo->padre = anterior;
    }else{
      anterior->der = nuevo;
      nuevo->padre = anterior;
    }
  }
}

void imprimirIn(struct nodo *reco){
  //Imprime el árbol siguiendo la forma inorden.
  if (reco != NULL){
    imprimirIn(reco->izq);
    printf("\n\nEl alias es: %s \n", reco->alias);
    printf("Y su url: %s \n",reco->url);
    imprimirIn(reco->der);
  }
}

void actualizarAlias(struct nodo *reco_b_u, char* url, char* nuevo_alias){
  //Busca el nodo con el url ingresado y actualiza su alias por nuevo_alias
  int son_iguales2;
  if (reco_b_u != NULL){
    actualizarAlias(reco_b_u->izq, url, nuevo_alias);
    son_iguales2 = strcmp(reco_b_u->url, url);
    if(son_iguales2 == 0){
      reco_b_u->alias = nuevo_alias;
      printf("\n\nEl alias se ha cambiado a: %s \n", reco_b_u->alias);
      printf("Y su url es: %s \n",reco_b_u->url);
    }
    actualizarAlias(reco_b_u->der, url, nuevo_alias);
  }
}

void buscar_alias(struct nodo* reco_b_a, char* alias){
  //Busca el nodo que tiene el alias de entrada y luego imprime su URL y alias
  int son_iguales;
  if (reco_b_a != NULL){
    buscar_alias(reco_b_a->izq, alias);
    son_iguales = strcmp(reco_b_a->alias, alias);
    if(son_iguales == 0){
      printf("\n\nEl alias es: %s \n", reco_b_a->alias);
      printf("Y su url: %s \n",reco_b_a->url);
      char *str = reco_b_a->url;
        abrirURL(str);
      }
      buscar_alias(reco_b_a->der, alias);
    }
}

void buscar_url(struct nodo *reco_b_u, char* url){
  //Busca el nodo que tiene el URL de entrada y luego imprime su URL y alias
  int son_iguales2;
  if (reco_b_u != NULL){
    buscar_url(reco_b_u->izq, url);
    son_iguales2 = strcmp(reco_b_u->url, url);
    if(son_iguales2 == 0){
      printf("\n\nEl alias es: %s \n", reco_b_u->alias);
      printf("Y su url: %s \n",reco_b_u->url);
      char *str = reco_b_u->url;
      abrirURL(str);
    }
    buscar_url(reco_b_u->der, url);
  }
}

void descargarArchivo(void){
  //Descarga el archivo de internet y cambia su nombre a "datos"
  system("wget https://moz.com/top-500/download/?table=top500Domains");
  system("mv index.html?table=top500Domains datos");
  printf("\n\n\n¡Archivo descargado con éxito!");
}

void insertarNodosDesdeArchivo(void){
  //Lee el archivo completo y va ingresando cada nodo con su respectivo URL y alias como "null"
  FILE* archivo_datos=NULL;
  char* nombre_archivo = "datos";
  archivo_datos = fopen(nombre_archivo, "r");  //Abre archivo en modo solo lectura
  
  if (archivo_datos != NULL){
    char linea[1024]; //en este char se va a almacenar una linea completa del archivo
    char *token;  
    int i = 0;  // filas - lineas
    while(fgets(linea, 1024, (FILE*) archivo_datos)){  //Recorre cada linea
      printf("\nLINEA: %s", linea);
      if (i!=0){ // "Si no es la primera linea del archivo"
        token = strtok(linea, "\","); //Token = Columna 0 (RANK)
        token = strtok(NULL, "\",");  //Token = Columna 1 (URL)
        printf("URL: %s\n", token);
        printf("ALIAS: NULL");
        printf("\n- Nodo Insertado -\n");
        insertar("null", token);  //Inserta Nodo en el arbol con el URL almacenado en token
      }
      i++;
    }    
  }
  printf("\n\nFIN DEL ARCHIVO\n\n");
  fclose(archivo_datos);    //Cierra el archivo
}

struct nodo *minimo_izquierda(struct nodo *arbol){
  //Busca el nodo de menor valor y lo retorna
  printf("\n>> z deseada: ");
  if(arbol == NULL){
    return NULL;
  }
  if(arbol->izq != NULL){
    return minimo_izquierda(arbol->izq );//Vuelve a llamar a la funcion hasta que llegue al elemento que este mas a la izquierda(el menor) y lo retorna
  }else{
    return arbol;
  }
}

void reemplazar(struct nodo *arbol, struct nodo * nuevo_n){
   //Reemplaza el valor de dos nodos 
  if(arbol->padre != NULL){
    if(arbol->url == arbol->padre->izq->url){
      arbol->padre->izq = nuevo_n;
    }else if(arbol->url == arbol->padre->der->url){
      arbol->padre->der = nuevo_n;
    }
  }
  if(nuevo_n != NULL){
    nuevo_n->padre = arbol->padre;
  }
}

void eliminar(struct nodo *nodo_e){
  //Elimina por completo un nodo del arbol
  nodo_e->izq = NULL;
  nodo_e->der = NULL;
  nodo_e->padre = NULL;
  free(nodo_e);
  printf("\n- Nodo Eliminado -\n");
}

void borrar_nodo(struct nodo *nodo_b){
  //Cambia valores y pocisiones de los nodos del arbol y despues lo elimina
  if(nodo_b->der != NULL && nodo_b->izq != NULL){
  nodo *menor = minimo_izquierda(nodo_b->der);
  nodo_b->url = menor->url;
  nodo_b->alias = menor->alias;
  borrar_nodo(menor);
  }else if(nodo_b->izq != NULL){ 
    reemplazar(nodo_b, nodo_b->izq);
    eliminar(nodo_b);
  }else if(nodo_b->der != NULL){ 
    reemplazar(nodo_b, nodo_b->der);
    eliminar(nodo_b);
  }else{
    reemplazar(nodo_b, NULL);
    eliminar(nodo_b);
  }
}

void borrar(struct nodo *reco_n, char* url){
  //Ubica el nodo que se desea eliminar
  int son_iguales2;
  if (reco_n != NULL){
    borrar(reco_n->izq, url);
    son_iguales2 = strcmp(reco_n->url, url);
    if(son_iguales2 == 0){
      borrar_nodo(reco_n);
    }
  borrar(reco_n->der, url);
  }
}

void menu(){
  size_t max_size;
  char *url_alias;
  char *nuevo_url;
  char *nuevo_alias;
  char *url;
  int opcion;
  do{
	printf("\n\n\n\n\n");
		printf("\n---------------------------------------------------------------------");
		printf("\n(0) Salir");
		printf("\n(1) Descargar lista de las mejores 500 paginas");
    printf("\n(2) Insertar nodos desde lista descargada");
    printf("\n(3) Insertar nodo de forma manual");
    printf("\n(4) Mostrar arbol");
		printf("\n(5) Buscar nodo por URL");
		printf("\n(6) Buscar nodo por alias");
    printf("\n(7) Eliminar nodo por URL");
    printf("\n(8) Actualizar alias de nodo");
    printf("\n(9) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		max_size = 1;
		opcion = get_user_numerical_input(max_size);
	switch(opcion){
      case 0: //Salir
        printf("\n Fin del programa \n");
        exit(0);

			case 1: //Descargar lista de las mejores 500 paginas
        descargarArchivo();
				break;

      case 2: //Insertar nodos desde lista descargada
        insertarNodosDesdeArchivo();
        break;

      case 3: //Insertar nodo de forma manual
        //Input del URL
        nuevo_url = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
	      printf("\n>> Ingrese el url: ");
	      max_size = LONGITUD_MAXIMA_NOMBRE;
        nuevo_url = get_user_input(max_size);

        //Input del alias
        nuevo_alias = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
	      printf("\n>> Ingrese el alias: ");
	      max_size = LONGITUD_MAXIMA_NOMBRE;
        nuevo_alias = get_user_input(max_size);
        insertar(nuevo_alias, nuevo_url);
        break;

			case 4: //Mostrar arbol
        imprimirIn(raiz);
        break;

      case 5: //Buscar nodo por URL
        //Input del URL
        url_alias = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
        printf("\n>> Ingrese el url: ");
        max_size = LONGITUD_MAXIMA_NOMBRE;
        url_alias = get_user_input(max_size);
        buscar_url(raiz, url_alias);
				break;

      case 6: //Buscar nodo por alias
        //Input del alias
        url_alias = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
        printf("\n>> Ingrese el alias: ");
        max_size = LONGITUD_MAXIMA_NOMBRE;
        url_alias = get_user_input(max_size);
        buscar_alias(raiz, url_alias);

        break;

      case 7: //Eliminar nodo por URL
        //Input del URL
        url_alias = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
        printf("\n>> Ingrese el url del nodo que quiere eliminar: ");
        max_size = LONGITUD_MAXIMA_NOMBRE;
        url_alias = get_user_input(max_size);
        borrar(raiz,url_alias);  
        break;

      case 8: //Actualizar alias de nodo
        //Input del URL
        url = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
        printf("\n>> Ingrese el url del nodo que quiere actualizar: ");
        max_size = LONGITUD_MAXIMA_NOMBRE;
        url = get_user_input(max_size);

        //Input del alias
        url_alias = (char *) malloc (LONGITUD_MAXIMA_NOMBRE);
        printf("\n>> Ingrese alias que le desea asignar: ");
        max_size = LONGITUD_MAXIMA_NOMBRE;
        url_alias = get_user_input(max_size);

        actualizarAlias(raiz, url, url_alias);
        break;


      case 9: //Salir
        printf("\n Fin del programa \n");
        exit(0);
    }    
  }while(opcion != 0);
}

int main(){
	menu();
	return 0;
}